#include "Player.hpp"


void Player::move()
{
    auto posToMove = diceRoller->roll();
    changePositionOnBoard(posToMove);
}

void Player::changePositionOnBoard(int posToMove)
{
    auto posToEnd = board->end() - posOnBoard;
    if(posToEnd < posToMove)
    {
        posOnBoard = board->begin() + (posToMove - posToEnd);
        return;
    }
    posOnBoard += posToMove;
}

bool Player::isBankrupt() const
{
    return money<=0;
}