#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "Board.hpp"
#include "DiceRoller.hpp"
#include <memory>

class Player
{
public:
    Player(int startMoney, std::shared_ptr<Board> board, std::shared_ptr<DiceRoller> diceRoller)
        : money(startMoney), board(board), diceRoller(diceRoller) {};
  //  Player& operator=(Player&&) = default;
    void move();
    bool isBankrupt() const;
private:
    void changePositionOnBoard(int posToMove);

    int money;
    Board::const_iterator posOnBoard;
    //Board boardRef;
   // DiceRoller diceRoller;
    std::shared_ptr<Board> board;
    std::shared_ptr<DiceRoller> diceRoller;
};

#endif // PLAYER_HPP