#ifndef BOARD_H
#define BOARD_H

#include <array>
#include "Square.hpp"

class Board
{
public:
    Board();

    using PlayingBoard = std::array<Square, 40>;
    using const_iterator = PlayingBoard::const_iterator;

    PlayingBoard::const_iterator begin() const
    {
        return playingBoard.begin();
    }

    PlayingBoard::const_iterator end() const
    {
        return playingBoard.end();
    }

    

private:
    void generateBoard();

    PlayingBoard playingBoard;
};

#endif // BOARD_H