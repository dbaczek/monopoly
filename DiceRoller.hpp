#ifndef DICE_H
#define DICE_H

#include <vector>

class DiceRoller
{
public:
    class Die
    {
    public:
        int roll() const
        {
            srand(time(0));
            int min = 1; 
            int max = 6;
            return rand() % (max - min + 1) + min;
        }
    };

    DiceRoller(int numOfDices)
    {
        for(int i=0; i<numOfDices; ++i)
        {
            dices.push_back(Die{});
        }
    };

    int roll() const
    {
        int result = 0;
        for(auto& dice : dices)
        {
            result += dice.roll();
        }
        return result;
    }
private:
    std::vector<Die> dices;
};

#endif // DICE_H