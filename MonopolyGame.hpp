#ifndef MONOPOLYGAME_H
#define MONOPOLYGAME_H

#include "Player.hpp"
#include "Board.hpp"
#include "DiceRoller.hpp"
#include <vector>
#include <memory>

class MonopolyGame
{
public:
    MonopolyGame(int noOfPlayers);
    void startGame();
private:
    void newRound();
    bool shouldContinue();

   // std::vector<std::unique_ptr<Player>> players;
    std::vector<Player> players;
   // std::shared_ptr<Board> board;
    //std::shared_ptr<DiceRoller> diceRoller;
};

#endif // MONOPOLYGAME_H