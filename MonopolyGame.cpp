#include "MonopolyGame.hpp"
#include <algorithm>

MonopolyGame::MonopolyGame(int noOfPlayers)
{
   // players(noOfPlayers, Player(200, board));
   auto diceRoller = std::make_shared<DiceRoller>(2);
   auto board = std::make_shared<Board>();
   for(auto i = 0; i < noOfPlayers; ++i)
   {
       players.push_back(Player(200, board, diceRoller));
   }
}

void MonopolyGame::startGame()
{
    while(shouldContinue())
    {
        newRound();
    }
}

bool MonopolyGame::shouldContinue()
{
    players.erase(
        std::remove_if(
            players.begin(), 
            players.end(),
            [](const Player& p) { return p.isBankrupt(); }
        ), 
        players.end()
    );

    return players.size() > 1;
}

void MonopolyGame::newRound()
{
    for(auto &player : players)
    {
        player.move();
    }
}