#pragma once

#include <iterator>

template<typename It>
struct container
{
	typedef	It::value_type	value_type;
	typedef It				iterator;
};

template<typename T>
class const_circular_iter : public std::iterator<std::bidirectional_iterator_tag, T::value_type>  
{
protected:
		T::iterator	begin;
		T::iterator	end;
		T::iterator	iter;
public:
	const_circular_iter(T& t) : iter(t.begin()), begin(t.begin()), end(t.end()) {};
	const_circular_iter(T::iterator b, T::iterator e) : iter(b), begin(b), end(e) {};

	const_circular_iter<T>& operator++()
	{	
		++iter;

		if (iter == end)
			iter = begin;

		return(*this);
	}

	const_circular_iter<T> operator++(int)
	{
		const_circular_iter<T>	t=*this;
		++(*this);
		return(t);
	}

	const T::value_type operator*() const 
		{return (*iter);}

	bool operator==(const const_circular_iter<T>&	rhs) const
		{return (iter == rhs.iter);}

	bool operator!=(const const_circular_iter<T>&	rhs) const 
		{return ! operator==(rhs); }
};
